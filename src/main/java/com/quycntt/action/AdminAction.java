package com.quycntt.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Category;
import com.quycntt.entity.Product;
import com.quycntt.service.ProductService;

/**
 * @author quycntt
 *
 */
public class AdminAction extends ActionSupport{
	private ProductService productService = new ProductService();
	private List<Product> listProduct;
	private String name;
	private String price;
	private int quantity;
	private String description;
	private String category;
	private int id;
	private Product product;
	private File image;
	private String imageContentType;
	private String imageFileName;
	private int start;
	private List<Product> listProductLimit;
	private int paging;
	private int numberPage;
	private String data;
	
	public static final int COUNT = 5;
	
	public List<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	}
	
	public List<Product> getListProductLimit() {
		return listProductLimit;
	}

	public void setListProductLimit(List<Product> listProductLimit) {
		this.listProductLimit = listProductLimit;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	
	public String getImageContentType() {
		return imageContentType;
	}
	
	public int getStart() {
		return start;
	}
	
	public void setStart(int start) {
		this.start = start;
	}

	public void setImageContentType(String imageContentType) {
		this.imageContentType = imageContentType;
	}
	
	public int getPaging() {
		return paging;
	}
	
	public void setPaging(int paging) {
		this.paging = paging;
	}
	
	public int getNumberPage() {
		return numberPage;
	}

	public void setNumberPage(int numberPage) {
		this.numberPage = numberPage;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String execute() throws Exception {
		listProduct = productService.findAll();
		double size = (double) listProduct.size();
		double page = Math.ceil(size / COUNT);
		paging = (int) page;
		if (numberPage <= 0) {
			numberPage = 1;
		}
		start = (numberPage - 1) * COUNT;
		listProductLimit = productService.findLimit(start, COUNT);
		return SUCCESS;
	}
	
	public String addexe() {
		return INPUT;
	}
	
	public String add() {
		try {
			String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("resources/images");
			File fileToCreate = new File(filePath, imageFileName);
			FileUtils.copyFile(image, fileToCreate);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Product product = new Product();
		product.setName(name);
		product.setPrice(price);
		product.setQuantity(quantity);
		product.setDescription(description);
		imageFileName = "/images/" + imageFileName;
		product.setImage(imageFileName);
		
		Category category1 = new Category();
		category1.setName(category);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		product.setCategory(category1);
		product.setJoindate(dateFormat.format(date));
		
		productService.addProduct(product);
		return SUCCESS;
	}
	
	public String updateexe() {
		product = productService.findOne(id);
		return INPUT;
	}
	
	public String update() {
		try {
			String filePath = ServletActionContext.getServletContext().getRealPath("/").concat("resources/images");
			File fileToCreate = new File(filePath, imageFileName);
			FileUtils.copyFile(image, fileToCreate);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		product = productService.findOne(id);
		product.setName(name);
		product.setQuantity(quantity);
		product.setDescription(description);
		
		Category category1 = new Category();
		category1.setName(category);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		product.setCategory(category1);
		product.setJoindate(dateFormat.format(date));
		
		imageFileName = "/images/" + imageFileName;
		product.setImage(imageFileName);
		
		productService.updateProduct(product);
		return SUCCESS;
	}
	
	public String deleteexe() {
		product = productService.findOne(id);
		productService.deleteProduct(product);
		return SUCCESS;
	}
}
