package com.quycntt.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Cart;
import com.quycntt.entity.Category;
import com.quycntt.entity.Invoice;
import com.quycntt.entity.InvoiceDetail;
import com.quycntt.entity.InvoiceDetailId;
import com.quycntt.service.CategoryService;
import com.quycntt.service.InvoiceDetailService;
import com.quycntt.service.InvoiceService;

public class CartAction extends ActionSupport{
	
	private CategoryService categoryService = new CategoryService();
	private InvoiceService invoiceService = new InvoiceService();
	private InvoiceDetailService invoiceDetailService = new InvoiceDetailService();
	private List<Category> listCategory;
	
	private String name;
	private String phone;
	private String address;
	private String delivery;
	private String note;

	public List<Category> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<Category> listCategory) {
		this.listCategory = listCategory;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String execute() throws Exception {
		listCategory = categoryService.findAll();
		return SUCCESS;
	}
	
	public String invoice() {
		Invoice invoice = new Invoice();
		invoice.setName(name);
		invoice.setPhone(phone);
		invoice.setAddress(address);
		invoice.setDelivery(delivery);
		invoice.setNote(note);
		
		invoiceService.addInvoice(invoice);
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		List<Cart> listCart = new ArrayList<Cart>();
		
		if (session.get("cart") != null) {
			listCart = (List<Cart>) session.get("cart");
		}
		
		for (Cart cart : listCart) {
			InvoiceDetailId invoiceDetailId = new InvoiceDetailId();
			invoiceDetailId.setIdinvoice(invoice.getId());
			invoiceDetailId.setIdproduct(cart.getId());
			
			InvoiceDetail invoiceDetail = new InvoiceDetail();
			invoiceDetail.setInvoiceDetailId(invoiceDetailId);
			invoiceDetail.setQuantity(cart.getQuantity());
			invoiceDetail.setPrice(cart.getPrice());
			
			invoiceDetailService.addInvoiceDetail(invoiceDetail);
		}
		
		return SUCCESS;
	}
}
