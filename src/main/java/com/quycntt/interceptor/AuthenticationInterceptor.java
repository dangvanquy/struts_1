package com.quycntt.interceptor;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.quycntt.entity.User;

public class AuthenticationInterceptor implements Interceptor{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void destroy() {
		
	}

	public void init() {
		
	}

	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext().getSession();
		User user = new User();
		
		if (session.get("user") != null) {
			user = (User) session.get("user");
		}
		
		if (user.getRole().getId() != 1 || user == null) {
			return ActionSupport.INPUT;
		}
		return invocation.invoke();
	}
}
