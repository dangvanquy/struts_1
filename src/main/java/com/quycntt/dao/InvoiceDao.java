package com.quycntt.dao;

import com.quycntt.entity.Invoice;

public interface InvoiceDao {
	boolean addInvoice(Invoice invoice);
}
