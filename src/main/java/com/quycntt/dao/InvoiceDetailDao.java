package com.quycntt.dao;

import com.quycntt.entity.InvoiceDetail;

public interface InvoiceDetailDao {
	boolean addInvoiceDetail(InvoiceDetail invoiceDetail);
}
