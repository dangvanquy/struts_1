package com.quycntt.service;

import com.quycntt.dao.InvoiceDao;
import com.quycntt.daoimp.InvoiceDaoImp;
import com.quycntt.entity.Invoice;

public class InvoiceService implements InvoiceDao{
	private InvoiceDaoImp invoiceDaoImp = new InvoiceDaoImp();
	
	public boolean addInvoice(Invoice invoice) {
		return invoiceDaoImp.addInvoice(invoice);
	}

}
