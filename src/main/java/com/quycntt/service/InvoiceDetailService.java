package com.quycntt.service;

import com.quycntt.dao.InvoiceDetailDao;
import com.quycntt.daoimp.InvoiceDetailDaoImp;
import com.quycntt.entity.InvoiceDetail;

public class InvoiceDetailService implements InvoiceDetailDao{
	private InvoiceDetailDaoImp invoiceDetailDaoImp = new InvoiceDetailDaoImp();
	
	public boolean addInvoiceDetail(InvoiceDetail invoiceDetail) {
		return invoiceDetailDaoImp.addInvoiceDetail(invoiceDetail);
	}

}
