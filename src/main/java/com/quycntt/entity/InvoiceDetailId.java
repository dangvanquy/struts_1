package com.quycntt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InvoiceDetailId implements Serializable{
	@Column
	private int idinvoice;
	
	@Column
	private int idproduct;

	public int getIdinvoice() {
		return idinvoice;
	}

	public void setIdinvoice(int idinvoice) {
		this.idinvoice = idinvoice;
	}

	public int getIdproduct() {
		return idproduct;
	}

	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}
}
