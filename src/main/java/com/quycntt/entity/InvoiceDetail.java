package com.quycntt.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="invoicedetail")
public class InvoiceDetail {
	@EmbeddedId
	private InvoiceDetailId invoiceDetailId;
	
	@Column
	private int quantity;
	
	@Column
	private String price;

	public InvoiceDetailId getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(InvoiceDetailId invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
}
