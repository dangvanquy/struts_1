package com.quycntt.daoimp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.quycntt.dao.CategoryDao;
import com.quycntt.entity.Category;

public class CategoryDaoImp implements CategoryDao{

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public List<Category> findAll() {
		Session session = null;
		List<Category> listCategory = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Category";
			listCategory = session.createQuery(sql).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return listCategory;
	}

}
