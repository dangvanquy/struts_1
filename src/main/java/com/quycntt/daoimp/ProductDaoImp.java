package com.quycntt.daoimp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.quycntt.dao.ProductDao;
import com.quycntt.entity.Product;

public class ProductDaoImp implements ProductDao{
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public List<Product> findLimit(int start, int count) {
		Session session = null;
		List<Product> listProductLimit = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Product order by joindate desc";
			listProductLimit = session.createQuery(sql).setFirstResult(start).setMaxResults(count).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return listProductLimit;
	}

	public Product findOne(int id) {
		Session session = null;
		Product product = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Product where id ="+id;
			product = (Product) session.createQuery(sql).uniqueResult();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return product;
	}

	public List<Product> findByCategory(int idcategory) {
		Session session = null;
		List<Product> listProduct = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Product where categoryid = "+idcategory;
			listProduct = session.createQuery(sql).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return listProduct;
	}

	public List<Product> findAll() {
		Session session = null;
		List<Product> listProduct = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Product order by joindate desc";
			
			listProduct = session.createQuery(sql).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return listProduct;
	}

	public boolean addProduct(Product product) {
		Session session = null;
		boolean check = false;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			int id = (Integer) session.save(product);
			if (id > 0) {
				check = true;
			} else {
				check = false;
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return check;
	}

	public void updateProduct(Product product) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(product);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
	}

	public void deleteProduct(Product product) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.delete(product);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
	}
}
