package com.quycntt.daoimp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.quycntt.dao.InvoiceDetailDao;
import com.quycntt.entity.InvoiceDetail;

public class InvoiceDetailDaoImp implements InvoiceDetailDao{
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public boolean addInvoiceDetail(InvoiceDetail invoiceDetail) {
		Session session = null;
		boolean check = false;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			int id = (Integer) session.save(invoiceDetail);
			
			if (id > 0) {
				check = true;
			} else {
				check = false;
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return check;
	}

}
