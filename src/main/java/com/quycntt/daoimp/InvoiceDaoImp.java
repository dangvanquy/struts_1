package com.quycntt.daoimp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.quycntt.dao.InvoiceDao;
import com.quycntt.entity.Invoice;

public class InvoiceDaoImp implements InvoiceDao{
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public boolean addInvoice(Invoice invoice) {
		Session session = null;
		boolean check = false;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			int id = (Integer) session.save(invoice);
			
			if (id > 0) {
				check = true;
			} else {
				check = false;
			}
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return check;
	}

}
