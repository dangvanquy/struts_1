<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>update</title>
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"/>
<link rel="stylesheet" href="resources/css/style.css"/>
</head>
<body>
<div class="container">
		<div class="login-form">
			<form action="update" method="post" enctype="multipart/form-data">
				<h2 class="text-center">Update</h2>
				<div class="form-group">
					<input class="form-control" name="id" readonly="true" value="<c:out value="${product.getId() }"/>" type="text" placeholder="ID"/>
				</div>
				<div class="form-group">
					<input class="form-control" name="name" value="<c:out value="${product.getName() }"/>" type="text" placeholder="Tên"/>
				</div>
				<div class="form-group">
					<input class="form-control" name="price" value="<c:out value="${product.getPrice() }"/>" type="text" placeholder="Giá"/>
				</div>
				<div class="form-group">
					<input class="form-control" name="quantity" value="<c:out value="${product.getQuantity() }"/>" type="text" placeholder="Số lượng"/>
				</div>
				<div class="form-group">
					<textarea class="form-control" name="description" rows="3"><c:out value="${product.getDescription() }"/></textarea>
				</div>
				<div class="form-group">
					<label>Danh mục</label>
					<select name="category">
						<option value="Samsung">Samsung</option>
						<option value="Oppo">Oppo</option>
						<option value="Iphone">Iphone</option>
					</select>
				</div>
				<div class="form-group">
					<label>Hình ảnh</label>
					<input name="image" type="file"/>
				</div>
				<div class="form-group">
					<button class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="resources/js/custom.js"></script>
	<script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>