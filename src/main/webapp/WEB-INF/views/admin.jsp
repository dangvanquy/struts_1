<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>admin</title>
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"/>
<link rel="stylesheet" href="resources/css/style.css"/>
</head>
<body>
<div class="container">

	<div class="icon-logout">
		<a href="logout"><i class="fa fa-sign-out-alt"></i></a>
	</div>
	<div class="row">
			<!-- List product -->
			<div id="list-product">
					 <div class="table-wrapper">
						<div class="table-title">
							<div class="row">
								<div class="col-md-5">
									<h2>List Product</h2>
								</div>
								<div class="col-md-5">
									
								</div>
								<div class="icon-add col-md-2">
									<a href="addexe"><i class="fas fa-plus-circle btn-add"></i><span>Add New</span></a>
								</div>
							</div>
						</div>
						<table id="table-product" class="table table-striped table-hover">
						<thead>
							<tr>
								<td>ID</td>
								<td>Name</td>
								<td>Price</td>
								<td>Quantity</td>
								<td class="td-img">Image</td>
								<td>Category</td>
								<td></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listProductLimit }" var="listProduct">
								<tr>
									<td><c:out value="${listProduct.getId() }"></c:out></td>
									<td><c:out value="${listProduct.getName() }" /></td>
									<td><c:out value="${listProduct.getPrice() }" /></td>
									<td><c:out value="${listProduct.getQuantity() }" /></td>
									<td class="td-img"><img src="resources<c:out value="${listProduct.getImage() }"/>" /></td>
									<td><c:out value="${listProduct.getCategory().getName() }" /></td>
									<td><a class="td-update" href="updateexe?id=<c:out value="${listProduct.getId() }" />"><i class="fas fa-pen"></i></a>
									<td><a class="td-delete" href="deleteexe?id=<c:out value="${listProduct.getId() }" />"><i class="fas fa-trash"></i></a>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					</div>
				<div class="paging">
					<nav aria-label="Page navigation example">
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
								<span class="sr-only">Previous</span>
						</a></li>
						
						<c:forEach var="i" begin="1" end="${paging }" >
							<c:choose>
								<c:when test="${i == 1 }">
									<li class="page-item btn-page active"><p class="page-link">${i }</p></li>
								</c:when>
								<c:otherwise>
									<li class="page-item btn-page"><p class="page-link">${i }</p></li>
								</c:otherwise>
							</c:choose>
							
						</c:forEach>
						
						<li class="page-item"><a class="page-link" href="#"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span
								class="sr-only">Next</span>
						</a></li>
					</ul>
					</nav>
				</div>
			</div>
			<!-- End list product -->
			
		</div>
</div>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="resources/js/custom.js"></script>
	<script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>